package ch.hsr.nikolastankovic.orderingsystem;

import ch.hsr.nikolastankovic.orderingsystem.models.*;
import ch.hsr.nikolastankovic.orderingsystem.utils.ColorUtils;

import java.util.ArrayList;

/**
 * This application prints an order with his items and details
 * to the command line.
 *
 * @author Nikola Stankovic, nikola.stankovic@hsr.ch
 * @version 1.0
 */
public class Main {

    private static ArrayList<Item> items = new ArrayList<Item>();
    private static Order myOrder;

    public static void main(String[] args) {

        initializeOrder();
        initializeOrderWithBundle();

        print();

    }

    /**
     * This method initialize some product and service items
     * to finally assign these items to an order.
     */
    private static void initializeOrder() {

        // assign the values to the items:
        ProductItem iPadAir2 = new ProductItem("Apple iPad Air 2 (9.70\", 64GB, Grau, Schwarz, WLAN)", 1, 559);
        ProductItem logitechKeyboard = new ProductItem("Logitech Keys-To-Go Keyboard (CH, Rot)", 1, 79);
        ProductItem adonitPen = new ProductItem("Adonit Jot Pro 2.0 (Schwarz)", 1, 39.15);
        ProductItem smartCase = new ProductItem("Apple iPad Air 2 Smart Case", 1, 79.50);
        ServiceItem warranty = new ServiceItem("Verlaengerung der Herstellergarantie auf 3 Jahre", 94.50);

        // add items to ArrayList items:
        items.add(iPadAir2);
        items.add(logitechKeyboard);
        items.add(adonitPen);
        items.add(smartCase);
        items.add(warranty);

        // send the items to the order:
        myOrder = new Order(items);

    }

    /**
     * This method initialize some product and service items
     * to finally assign these items to a bundle. This bundle
     * is finally added as item to the existing order.
     */
    private static void initializeOrderWithBundle() {

        ArrayList<Item> itemsForBundle = new ArrayList<Item>();

        ProductItem macBookAir13 = new ProductItem("Apple MacBook Pro Retina (13.30\", WQXGA, Intel Core i5, 8GB)", 1, 1429);
        ServiceItem protectionPlan = new ServiceItem("Apple Care Protection Plan (F) MB Air 11-13\" / 13\" Pro", 249);
        ProductItem sleeve = new ProductItem("Hama Notebook Sleeve Comfort (13.30\", Schwarz)", 1, 25.90);

        itemsForBundle.add(macBookAir13);
        itemsForBundle.add(protectionPlan);
        itemsForBundle.add(sleeve);

        short discount = 35;
        String description = "Student MacBook Air 13'' Starter Kit";
        Bundle myBundle = new Bundle(itemsForBundle, discount, description);

        myOrder.add(myBundle);
    }

    /**
     * This method prints all given information of the order
     * and his items.
     */
    private static void print() {
        System.out.println(ColorUtils.ANSI_BLUE + "Total Preis: " + ColorUtils.ANSI_RESET
                + myOrder.getTotalPrice() + "\n");

        myOrder.printItems();

    }
}
