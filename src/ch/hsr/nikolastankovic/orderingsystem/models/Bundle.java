package ch.hsr.nikolastankovic.orderingsystem.models;

import ch.hsr.nikolastankovic.orderingsystem.utils.ColorUtils;

import java.util.ArrayList;

/**
 * Represents a bundle. This model contains items and the
 * property discount.
 */
public class Bundle extends Item{
    private ArrayList<Item> items = new ArrayList<Item>();
    private short discount;

    /**
     * Constructor for a bundle with all information.
     *
     * @param items
     * @param discount
     * @param description
     */
    public Bundle(ArrayList<Item> items, short discount, String description) {
        this.setDescription(description);
        this.items = items;
        this.discount = discount;
    }

    /**
     * Returns the price of the whole bundle. Calculated with every
     * single item and the given discount.
     *
     * @return the total price with discount of the bundle
     */
    public double getPrice() {
        double price = 0;

        for (int counter = 0; counter < items.size(); counter++) {
            price = price + this.items.get(counter).getPrice();
        }

        double runningTotal = (100 - discount)/ (double)100;
        price = runningTotal * price;

        return price;
    }

    /**
     * This method prints all given information of the bundle
     * and his items.
     */
    public void print() {
        System.out.println(ColorUtils.ANSI_BLUE + "Bezeichnung: " + ColorUtils.ANSI_RESET + getDescription());
        System.out.println(ColorUtils.ANSI_BLUE + "Rabatt: " + ColorUtils.ANSI_RESET + discount);

        for (int counter = 0; counter < items.size(); counter++) {
            int itemNumber = counter + 1;
            System.out.println(ColorUtils.ANSI_BLUE + "Item number: " + ColorUtils.ANSI_RESET + itemNumber);
            items.get(counter).print();
        }
    }
}
