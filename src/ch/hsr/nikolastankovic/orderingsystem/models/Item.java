package ch.hsr.nikolastankovic.orderingsystem.models;

/**
 * Abstract model item.
 */
public abstract class Item {

    private String description;

    /**
     * Sets the description of an item.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the description of an item.
     *
     * @return the description of an item.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Abstract method for returning the price of an item.
     *
     * @return the price of an item
     */
    public abstract double getPrice();

    /**
     * Abstract method for printing all given information of the item.
     */
    public abstract void print();
}
