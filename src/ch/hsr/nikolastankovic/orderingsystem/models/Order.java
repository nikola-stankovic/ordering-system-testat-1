package ch.hsr.nikolastankovic.orderingsystem.models;

import ch.hsr.nikolastankovic.orderingsystem.utils.ColorUtils;

import java.util.ArrayList;

/**
 * Represents an order. This model contains items.
 */
public class Order {

    private ArrayList<Item> items;

    /**
     * Constructor for an order with all items.
     *
     * @param items
     */
    public Order(ArrayList<Item> items) {
        this.items = items;
    }

    /**
     * Adds a new item to the existing item set.
     *
     * @param item
     */
    public void add(Item item) {

        if (item instanceof Bundle)  {
            for (int counter = 1; counter < items.size(); counter++) {
                if (item == items.get(counter)) {
                    throw new RuntimeException("Das gleiche Bundle kann nicht im selben Bundle hinzugefügt werden!");
                }
            }
        }

        items.add(item);
    }

    /**
     * Returns the price of the whole order. Calculated with
     * every single item.
     *
     * @return the total price of the order
     */
    public double getTotalPrice() {
        double totalPrice = 0;

        for (int counter = 0; counter < items.size(); counter++) {
            totalPrice = totalPrice + items.get(counter).getPrice();
        }

        return totalPrice;
    }

    /**
     * This method prints all given information of the order
     * and his items.
     */
    public void printItems() {
        for (int counter = 0; counter < items.size(); counter++) {
            int itemNumber = counter + 1;
            System.out.println(ColorUtils.ANSI_BLUE + "Item number: " + ColorUtils.ANSI_RESET + itemNumber);
            items.get(counter).print();
        }
    }
}
