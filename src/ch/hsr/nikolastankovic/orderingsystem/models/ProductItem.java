package ch.hsr.nikolastankovic.orderingsystem.models;

import ch.hsr.nikolastankovic.orderingsystem.utils.ColorUtils;

/**
 * Represents a product item. This model contains all information about a product item.
 */
public class ProductItem extends Item {

    private int amount;
    private double pricePerUnit;

    /**
     * Constructor for a product item with all details.
     *
     * @param description name of the product item
     * @param amount amount of the product item ordered
     * @param pricePerUnit price per unit of the product item
     */
    public ProductItem(String description, int amount, double pricePerUnit) {
        this.setDescription(description);
        this.amount = amount;
        this.pricePerUnit = pricePerUnit;
    }

    /**
     * Returns the price of a product item.
     *
     * @return the price of a product item
     */
    public double getPrice() {
        double price = pricePerUnit * amount;
        return price;
    }

    /**
     * Prints out all given information of the product item.
     */
    @Override
    public void print() {
        System.out.println(ColorUtils.ANSI_BLUE + "Bezeichnung: " + ColorUtils.ANSI_RESET + getDescription());
        System.out.println(ColorUtils.ANSI_BLUE + "Anzahl: " + ColorUtils.ANSI_RESET + amount);
        System.out.println(ColorUtils.ANSI_BLUE + "Preis pro Stueck: " + ColorUtils.ANSI_RESET + pricePerUnit);
        System.out.println();
    }
}
