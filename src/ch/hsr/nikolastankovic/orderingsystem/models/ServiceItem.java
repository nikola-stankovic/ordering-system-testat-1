package ch.hsr.nikolastankovic.orderingsystem.models;

import ch.hsr.nikolastankovic.orderingsystem.utils.ColorUtils;

/**
 * Represents a service item. This model contains all information about a service item.
 */
public class ServiceItem extends Item {

    private double price;

    /**
     * Constructor for a service item with all details.
     *
     * @param description name of the service item
     * @param price price of the service item
     */
    public ServiceItem(String description, double price) {
        this.setDescription(description);
        this.price = price;
    }

    /**
     * Returns the price of a service item.
     *
     * @return the price of a service item
     */
    @Override
    public double getPrice() {
        return price;
    }

    /**
     * Prints out all given information of the service item.
     */
    @Override
    public void print() {
        System.out.println(ColorUtils.ANSI_BLUE + "Bezeichnung: " + ColorUtils.ANSI_RESET + getDescription());
        System.out.println(ColorUtils.ANSI_BLUE + "Preis: " + ColorUtils.ANSI_RESET + price);
        System.out.println();
    }
}
